(function() {
    'use strict';

    // init the Open-Display API module
    angular.module('sh.api', [
        'ngCookies'
    ]);
})();