(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shInfotainment', InfotainmentFactory);

    /**
     * @ngdoc service 
     * @name sh.api.shInfotainment
     * @description
     *
     * The `shInfotainment` service provides a convenient wrapper for infotainment related requests.
     *
     */

    /* @ngInject */
    function InfotainmentFactory(shApi) {

        var service = {
            create: create,
            list: list,
            remove: remove,
            getSources:getSources,
            listItems:listItems,
            approve:approve,
            rejected:rejected,
            restore:restore,
            feature:feature,
            aircheckrCountries: aircheckrCountries,
            aircheckrLocations: aircheckrLocations,
            getStocks: getStocks,
            currenciesCombinationActive: currenciesCombinationActive,
            currencies: currencies,
            datacallRequest:datacallRequest
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return shApi.request('/0/Infotainment/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return shApi.request('/0/Infotainment/List', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return shApi.request('/0/Infotainment/Remove', data);
        }


        /**
         * @ngdoc method
         * @name listItems
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function listItems(data) {
            return shApi.request('/0/Infotainment/ListItems', data);
        }
        
        /**
         * @ngdoc method
         * @name getSources
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function getSources(data) {
            return shApi.request('/0/Infotainment/GetSources', data);
        }

        /**
         * @ngdoc method
         * @name approve
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function approve(data) {
            return shApi.request('/0/Infotainment/Approve', data);
        }

        /**
         * @ngdoc method
         * @name reject
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function rejected(data) {
            return shApi.request('/0/Infotainment/Rejected', data);
        }

        /**
         * @ngdoc method
         * @name restore
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function restore(data) {
            return shApi.request('/0/Infotainment/Restore', data);
        }

        /**
         * @ngdoc method
         * @name feature
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function feature(data) {
            return shApi.request('/0/Infotainment/Feature', data);
        }

        /**
         * @ngdoc method
         * @name aircheckrCountries
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function aircheckrCountries(data) {
            return shApi.request('/0/Infotainment/AircheckrCountries', data);
        }
        
        /**
         * @ngdoc method
         * @name aircheckrLocations
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function aircheckrLocations(data) {
            return shApi.request('/0/Infotainment/AircheckrLocations', data);
        }
        /**
         * @ngdoc method
         * @name GetStocks
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function getStocks(data) {
            return shApi.request('/0/Infotainment/GetStocks', data);
        }
        /**
         * @ngdoc method
         * @name CurrenciesCombinationActive
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function currenciesCombinationActive(data) {
            return shApi.request('/0/Infotainment/CurrenciesCombinationActive', data);
        }
        /**
         * @ngdoc method
         * @name Currencies
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function currencies(data) {
            return shApi.request('/0/Infotainment/Currencies', data);
        }
        /**
         * @ngdoc method
         * @name DatacallRequest
         * @methodOf sh.api.shInfotainment
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function datacallRequest(data) {
            return shApi.request('/0/Infotainment/DatacallRequest', data);
        }
    }
})();
