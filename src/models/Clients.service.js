(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shClients', ClientsFactory);

    /**
     * @ngdoc service 
     * @name sh.api.shClients
     * @description
     *
     * The `shClients` service provides a convenient wrapper for subusers related requests.
     *
     */

    /* @ngInject */
    function ClientsFactory(shApi) {

        var service = {
            create: create,
            list: list,
            remove: remove,
            restore:restore,
            update: update,
            resetPassword:resetPassword,
            updateSettings: updateSettings,
            updateInfo: updateInfo,
            getAccountInfo: getAccountInfo,
            getAccountSettings: getAccountSettings,
            updateTrial: updateTrial,
            updateLiveHours: updateLiveHours,
            updateExtraFeeds: updateExtraFeeds,
            move: move,
            getInvoiceURL:getInvoiceURL,
            getLoginLog:getLoginLog,
            getActionLog:getActionLog,
            getInfotainmentStats:getInfotainmentStats,
            saveClientSettings:saveClientSettings,
            getClientSettings:getClientSettings,
            changeLicensesAmount:changeLicensesAmount,
            listDisplays: listDisplays,
            setDisplayState: setDisplayState,
            removeDisplay: removeDisplay,
            renameDisplay: renameDisplay,
            listSubusers: listSubusers,
            updateAppspacePlan: updateAppspacePlan
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return shApi.request('/1/Clients/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will list all sub users.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return shApi.request('/1/Clients/List', data);
        }


        /**
         * @ngdoc method
         * @name remove
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to remove the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return shApi.request('/1/Clients/Remove', data);
        }

        /**
         * @ngdoc method
         * @name restore
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to restore the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function restore(data) {
            return shApi.request('/1/Clients/Restore', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return shApi.request('/1/Clients/Update', data);
        }

         /**
         * @ngdoc method
         * @name update
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/resetPassword
         * ```
         *
         * @param {Object} data data options object.
         */
        function resetPassword(data) {
            return shApi.request('/1/Clients/ResetPassword', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/resetPassword
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateSettings(data) {
            return shApi.request('/1/Clients/UpdateSettings', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/resetPassword
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateInfo(data) {
            return shApi.request('/1/Clients/UpdateInfo', data);
        }

        /**
         * @ngdoc method
         * @name getAccountInfo
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to getAccountInfo the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function getAccountInfo(data) {
            return shApi.request('/1/Clients/GetAccountInfo', data);
        }

        /**
         * @ngdoc method
         * @name getAccountSettings
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to getAccountSettings the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function getAccountSettings(data) {
            return shApi.request('/1/Clients/GetAccountSettings', data);
        }

        /**
         * @ngdoc method
         * @name updateTrial
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to updateTrial the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateTrial(data) {
            return shApi.request('/1/Clients/UpdateTrial', data);
        }

        /**
         * @ngdoc method
         * @name updateLiveHours
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to updateLiveHours the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateLiveHours(data) {
            return shApi.request('/1/Clients/UpdateLiveHours', data);
        }

        /**
         * @ngdoc method
         * @name updateExtraFeeds
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to updateExtraFeeds the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateExtraFeeds(data) {
            return shApi.request('/1/Clients/UpdateExtraFeeds', data);
        }

        /**
         * @ngdoc method
         * @name move
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to move the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function move(data) {
            return shApi.request('/1/Clients/Move', data);
        }

        /**
         * @ngdoc method
         * @name getInvoiceURL
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to getInvoiceURL the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function getInvoiceURL(data) {
            return shApi.request('/1/Clients/GetInvoiceURL', data);
        }

        /**
         * @ngdoc method
         * @name getInvoiceURL
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to getInvoiceURL the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function getLoginLog(data) {
            return shApi.request('/1/Clients/GetLoginLog', data);
        }

        /**
         * @ngdoc method
         * @name getInvoiceURL
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to getInvoiceURL the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function getActionLog(data) {
            return shApi.request('/1/Clients/GetActionLog', data);
        }

        /**
         * @ngdoc method
         * @name GetInfotainmentStats
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to GetInfotainmentStats the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function getInfotainmentStats(data) {
            return shApi.request('/1/Clients/GetInfotainmentStats', data);
        }

        /**
         * @ngdoc method
         * @name SaveClientSettings
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to SaveClientSettings the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function saveClientSettings(data) {
            return shApi.request('/1/Clients/SaveClientSettings', data);
        }

        /**
         * @ngdoc method
         * @name GetClientSettings
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to GetClientSettings the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function getClientSettings(data) {
            return shApi.request('/1/Clients/GetClientSettings', data);
        }

        /**
         * @ngdoc method
         * @name ChangeLicensesAmount
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to ChangeLicensesAmount.
         *
         * ```js
         * ```
         *
         * @param {Object} data data options object.
         */
        function changeLicensesAmount(data) {
            return shApi.request('/1/Clients/updatelicenses', data);
        }

        /**
         * @ngdoc method
         * @name listDisplays
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to listDisplays.
         *
         * ```js
         * ```
         *
         * @param {Object} data data options object.
         */
        function listDisplays(data) {
            return shApi.request('/1/Clients/ListDisplays', data);
        }
        /**
         * @ngdoc method
         * @name setDisplayState
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to setDisplayState.
         *
         * ```js
         * ```
         *
         * @param {Object} data data options object.
         */
        function setDisplayState(data) {
            return shApi.request('/1/Clients/SetDisplayState', data);
        }
        /**
         * @ngdoc method
         * @name removeDisplay
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to removeDisplay.
         *
         * ```js
         * ```
         *
         * @param {Object} data data options object.
         */
        function removeDisplay(data) {
            return shApi.request('/1/Clients/RemoveDisplay', data);
        }
        /**
         * @ngdoc method
         * @name renameDisplay
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to renameDisplay.
         *
         * ```js
         * ```
         *
         * @param {Object} data data options object.
         */
        function renameDisplay(data) {
            return shApi.request('/1/Clients/RenameDisplay', data);
        }
        /**
         * @ngdoc method
         * @name listSubusers
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to listSubusers.
         *
         * ```js
         * ```
         *
         * @param {Object} data data options object.
         */
        function listSubusers(data) {
            return shApi.request('/1/Clients/ListSubusers', data);
        }

        /**
         * @ngdoc method
         * @name UpdateAppspacePlan
         * @methodOf sh.api.shClients
         *
         * @description
         * This function will allow you to UpdateAppspacePlan.
         *
         * ```js
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateAppspacePlan(data) {
            return shApi.request('/1/Clients/UpdateAppspacePlan', data);
        }
    }
})();
