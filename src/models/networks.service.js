(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shNetworks', NetworksFactory);

    /**
     * @ngdoc service 
     * @name sh.api.shNetworks
     * @description
     *
     * The `shNetworks` service provides a convenient wrapper for subusers related requests.
     *
     */

    /* @ngInject */
    function NetworksFactory(shApi) {

        var service = {
            create: create,
            list: list,
            remove: remove,
            update: update
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf sh.api.shNetworks
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Networks/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return shApi.request('/0/Networks/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf sh.api.shNetworks
         *
         * @description
         * This function will list all sub users.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Networks/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return shApi.request('/0/Networks/List', data);
        }


        /**
         * @ngdoc method
         * @name remove
         * @methodOf sh.api.shNetworks
         *
         * @description
         * This function will allow you to remove the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Networks/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return shApi.request('/0/Networks/Remove', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf sh.api.shNetworks
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Networks/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return shApi.request('/0/Networks/Update', data);
        }
    }
})();
