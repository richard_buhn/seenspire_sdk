(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shDisplay', DisplayFactory);

    /**
     * @ngdoc service 
     * @name sh.api.shDisplay
     * @description
     *
     * The `shDisplay` service provides a convenient wrapper for subusers related requests.
     *
     */

    /* @ngInject */
    function DisplayFactory(shApi) {

        var service = {
            remove: remove,
            setState: setState,
            rename: rename,
            list: list
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name remove
         * @methodOf sh.api.shDisplay
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return shApi.request('/0/Display/Remove', data);
        }

         /**
         * @ngdoc method
         * @name setState
         * @methodOf sh.api.shDisplay
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function setState(data) {
            return shApi.request('/0/Display/SetState', data);
        }

         /**
         * @ngdoc method
         * @name rename
         * @methodOf sh.api.shDisplay
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function rename(data) {
            return shApi.request('/0/Display/Rename', data);
        }

         /**
         * @ngdoc method
         * @name list
         * @methodOf sh.api.shDisplay
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Clients/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return shApi.request('/0/Display/List', data);
        }
    }
})();
