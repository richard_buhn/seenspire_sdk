(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shLog', logFactory);

    /**
     * @ngdoc service 
     * @name sh.api.shLog
     * @description
     *
     * The `shLog` service provides a convenient wrapper for log related requests.
     *
     */

    /* @ngInject */
    function logFactory(shApi) {

        var service = {
            list: list,
            login: login
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name list
         * @methodOf sh.api.shLog
         *
         * @description
         * This function will allow you to list all actions.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Log/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return shApi.request('/0/Log/List', data);
        }

        /**
         * @ngdoc method
         * @name login
         * @methodOf sh.api.shLog
         *
         * @description
         * This function will allow you to list the logins.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Log/Login
         * ```
         *
         * @param {Object} data data options object.
         */
        function login(data) {
            return shApi.request('/0/Log/Login', data);
        }
    }
})();
