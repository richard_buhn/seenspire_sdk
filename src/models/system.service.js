(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shSystem', SystemFactory);

    /**
     * @ngdoc service 
     * @name sh.api.shSystem
     * @description
     *
     * The `shSystem` service provides a convenient wrapper for subusers related requests.
     *
     */

    /* @ngInject */
    function SystemFactory(shApi) {

        var service = {
            get: get,
            update: update
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name update
         * @methodOf sh.api.shSystem
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return shApi.request('/1/System/Update', data);
        }

        /**
         * @ngdoc method
         * @name get
         * @methodOf sh.api.shSystem
         *
         * @description
         * This function will list all sub users.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function get(data) {
            return shApi.request('/1/System/Get', data);
        }
    }
})();
