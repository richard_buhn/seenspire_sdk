(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shApplication', applicationFactory);

    /**
     * @ngdoc service 
     * @name sh.api.shApplication
     * @description
     *
     * The `shApplication` service provides a convenient wrapper for app related requests.
     *
     */

    /* @ngInject */
    function applicationFactory(shApi) {

        var service = {
            accessLevels: accessLevels,
            categories: categories,
            create: create,
            devList: devList,
            list: list,
            remove: remove,
            revokeAccess: revokeAccess,
            setGroup: setGroup,
            update: update,
            generateLongLivedToken: generateLongLivedToken
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name accessLevels
         * @methodOf sh.api.shApplication
         *
         * @description
         * This function gives you a list of access rules that an application can have.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Applications/AccessLevels
         * ```
         *
         * @param {Object} data data options object.
         */
        function accessLevels(data) {
            return shApi.request('/0/Applications/AccessLevels', data);
        }

        /**
         * @ngdoc method
         * @name categories
         * @methodOf sh.api.shApplication
         *
         * @description
         * 
         *
         * ```js
         * https://api.open-display.io/webapi/core/Applications/Categories
         * ```
         *
         * @param {Object} data data options object.
         */
        function categories(data) {
            return shApi.request('/0/Applications/Categories', data);
        }

        /**
         * @ngdoc method
         * @name create
         * @methodOf sh.api.shApplication
         *
         * @description
         * This function lets you create a custom application.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Applications/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return shApi.request('/0/Applications/New', data);
        }

        /**
         * @ngdoc method
         * @name devList
         * @methodOf sh.api.shApplication
         *
         * @description
         * This function will list all custom create applications.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Applications/DevList
         * ```
         *
         * @param {Object} data data options object.
         */
        function devList(data) {
            return shApi.request('/0/Applications/DevList', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf sh.api.shApplication
         *
         * @description
         * This function list all applications that you have allowed access you your account data. 
         * The access can be revoked at anytime. the default system applications can not be revoked.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Applications/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return shApi.request('/0/Applications/List', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf sh.api.shApplication
         *
         * @description
         * This function will let you remove the custom application that is located in the users account.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Applications/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return shApi.request('/0/Applications/Remove', data);
        }

        /**
         * @ngdoc method
         * @name revokeAccess
         * @methodOf sh.api.shApplication
         *
         * @description
         * Every application except the default application can have their access revoked. This gives the user control over his data and who can access it.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Applications/RevokeAccess
         * ```
         *
         * @param {Object} data data options object.
         */
        function revokeAccess(data) {
            return shApi.request('/0/Applications/RevokeAccess', data);
        }

        /**
         * @ngdoc method
         * @name setGroup
         * @methodOf sh.api.shApplication
         *
         * @description
         * With this function you will be able to grant access to a list of groups. A subuser can be in a group that has access to a specified application.
         *
         * So this function is used to allow subusers to a specified application.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Applications/SetGroup
         * ```
         *
         * @param {Object} data data options object.
         */
        function setGroup(data) {
            return shApi.request('/0/Applications/SetGroup', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf sh.api.shApplication
         *
         * @description
         * This function lets you update the custom applications settings.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Applications/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return shApi.request('/0/Applications/Update', data);
        }

          /**
         * @ngdoc method
         * @name generateLongLivedToken
         * @methodOf sh.api.shApplication
         *
         * @description
         * This function lets you update the custom applications settings.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Applications/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function generateLongLivedToken(data) {
            return shApi.request('/0/Applications/GenerateLongLivedToken', data);
        }
    }
})();
