(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shHub', HubFactory);

    /**
     * @ngdoc service 
     * @name sh.api.shHub
     * @description
     *
     * The `shHub` service provides a convenient wrapper for subusers related requests.
     *
     */

    /* @ngInject */
    function HubFactory(shApi) {

        var service = {
            create: create,
            list: list,
            remove: remove,
            update: update,
            info: info,
            listBasic: listBasic,
            listStats: listStats,
            listConnections: listConnections,
            newConnection: newConnection,
            removeConnection: removeConnection,
            updateConnection: updateConnection,
            getBadWords: getBadWords,
            updateBadWords: updateBadWords,
            twitterAutocomplete: twitterAutocomplete
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf sh.api.shHub
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return shApi.request('/0/Hub/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf sh.api.shHub
         *
         * @description
         * This function will list all sub users.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return shApi.request('/0/Hub/List', data);
        }


        /**
         * @ngdoc method
         * @name remove
         * @methodOf sh.api.shHub
         *
         * @description
         * This function will allow you to remove the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return shApi.request('/0/Hub/Remove', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf sh.api.shHub
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return shApi.request('/0/Hub/Update', data);
        }

        /**
         * @ngdoc method
         * @name info
         * @methodOf sh.api.shHub
         *
         * @description
         * This function will info the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/Info
         * ```
         *
         * @param {Object} data data options object.
         */
        function info(data) {
            return shApi.request('/0/Hub/Info', data);
        }

         /**
         * @ngdoc method
         * @name info
         * @methodOf sh.api.shHub
         *
         * @description
         * This function will return channels statistics.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/Info
         * ```
         *
         * @param {Object} data data options object.
         */
        function listStats(data) {
            return shApi.request('/0/Hub/ListStats', data);
        }

        /**
         * @ngdoc method
         * @name info
         * @methodOf sh.api.shHub
         *
         * @description
         * This function will return all channels names and ids.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/Info
         * ```
         *
         * @param {Object} data data options object.
         */
        function listBasic(data) {
            return shApi.request('/0/Hub/ListBasic', data);
        }

        /**
         * @ngdoc method
         * @name info
         * @methodOf sh.api.shHub
         *
         * @description
         * This function will info the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/Info
         * ```
         *
         * @param {Object} data data options object.
         */
        function listConnections(data) {
            return shApi.request('/0/Hub/ListConnections', data);
        }

         /**
         * @ngdoc method
         * @name info
         * @methodOf sh.api.shHub
         *
         * @description
         * This function will info the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/Info
         * ```
         *
         * @param {Object} data data options object.
         */
        function newConnection(data) {
            return shApi.request('/0/Hub/NewConnection', data);
        }


        /**
         * @ngdoc method
         * @name info
         * @methodOf sh.api.shHub
         *
         * @description
         * This function will info the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/Info
         * ```
         *
         * @param {Object} data data options object.
         */
        function removeConnection(data) {
            return shApi.request('/0/Hub/RemoveConnection', data);
        }

        /**
         * @ngdoc method
         * @name info
         * @methodOf sh.api.shHub
         *
         * @description
         * This function will info the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/UpdateConnection
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateConnection(data) {
            return shApi.request('/0/Hub/UpdateConnection', data);
        }

        /**
         * @ngdoc method
         * @name info
         * @methodOf sh.api.shHub
         *
         * @description
         * This function will info the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/getBadWords
         * ```
         *
         * @param {Object} data data options object.
         */
        function getBadWords(data) {
            return shApi.request('/0/Hub/GetBadWords', data);
        }

        /**
         * @ngdoc method
         * @name info
         * @methodOf sh.api.shHub
         *
         * @description
         * This function will info the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/updateBadWords
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateBadWords(data) {
            return shApi.request('/0/Hub/UpdateBadWords', data);
        }

        /**
         * @ngdoc method
         * @name twitterAutocomplete
         * @methodOf sh.api.shHub
         *
         * @description
         * Get list of twitter users based on query value
         *
         * ```js
         * https://api.open-display.io/webapi/core/Hub/TwitterAutocomplete
         * ```
         *
         * @param {Object} data data options object.
         */
        function twitterAutocomplete(data) {
            return shApi.request('/0/Hub/TwitterAutocomplete', data);
        }

    }
})();
