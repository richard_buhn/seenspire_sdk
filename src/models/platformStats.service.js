(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shPlatformStats', PlatformStatsFactory);

    /**
     * @ngdoc service 
     * @name sh.api.shPlatformStats
     * @description
     *
     * The `shPlatformStats` service provides a convenient wrapper for subusers related requests.
     *
     */

    /* @ngInject */
    function PlatformStatsFactory(shApi) {

        var service = {
            global: global,
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name global
         * @methodOf sh.api.shPlatformStats
         *
         * @description
         * This function will allow you skipItem a sub user, you do need to skipItem a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function global(data) {
            return shApi.request('/1/PlatformStats/Global', data);
        }
    }
})();
