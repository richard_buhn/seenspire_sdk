(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shUGC', UGCFactory);

    /**
     * @ngdoc service
     * @name sh.api.shUGC
     * @description
     *
     * The `shUGC` service provides a convenient wrapper for subusers related requests.
     *
     */

    /* @ngInject */
    function UGCFactory(shApi) {

        var service = {
            approve:approve,
            decline:decline,
            list:list,
            check:check
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name approve
         * @methodOf sh.api.shItems
         *
         * @description
         * This function will approve a ugc request.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Items/Approve
         * ```
         *
         * @param {Object} data data options object.
         */
        function approve(data) {
            return shApi.request('/1/UGC/Approve', data);
        }

        /**
         * @ngdoc method
         * @name decline
         * @methodOf sh.api.shUGC
         *
         * @description
         * This function will decline a ugc request
         *
         * ```js
         * https://api.open-display.io/webapi/core/UGC/Decline
         * ```
         *
         * @param {Object} data data options object.
         */
        function decline(data) {
            return shApi.request('/1/UGC/Decline', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf sh.api.shUGC
         *
         * @description
         * This function will list all ugc entries
         * use filters
         *
         * ```js
         * https://api.open-display.io/webapi/core/UGC/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return shApi.request('/1/UGC/List', data);
        }

        /**
         * @ngdoc method
         * @name check
         * @methodOf sh.api.shUGC
         *
         * @description
         * This function will insert the current time as CheckTimestamp
         *
         * ```js
         * https://api.open-display.io/webapi/core/UGC/Check
         * ```
         *
         * @param {Object} data data options object.
         */
        function check(data) {
            return shApi.request('/1/UGC/Check', data);
        }
    }
})();
