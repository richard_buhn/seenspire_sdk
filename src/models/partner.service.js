(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shPartner', PartnerFactory);

    /**
     * @ngdoc service 
     * @name sh.api.shPartner
     * @description
     *
     * The `shPartner` service provides a convenient wrapper for subusers related requests.
     *
     */

    /* @ngInject */
    function PartnerFactory(shApi) {

        var service = {
            create: create,
            list: list,
            remove: remove,
            update: update,
            stats: stats,
            addUser: addUser,
            removeUser: removeUser,
            updateUser: updateUser,
            listUsers: listUsers,
            updateBranding: updateBranding,
            getBranding: getBranding,
            getReports :getReports,
            getReportInfo: getReportInfo,
            changeReportStatus: changeReportStatus,
            getSourceStats: getSourceStats,
            createOrder: createOrder,
            changeOrderState: changeOrderState,
            listOrders: listOrders,
            getAppspacePlans: getAppspacePlans
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name create
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will allow you create a sub user, you do need to create a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return shApi.request('/1/Partner/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will list all sub users.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return shApi.request('/1/Partner/List', data);
        }


        /**
         * @ngdoc method
         * @name remove
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will allow you to remove the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return shApi.request('/1/Partner/Remove', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function update(data) {
            return shApi.request('/1/Partner/Update', data);
        }

        /**
         * @ngdoc method
         * @name stats
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function stats(data) {
            return shApi.request('/1/Partner/Stats', data);
        }

        /**
         * @ngdoc method
         * @name stats
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function addUser(data) {
            return shApi.request('/1/Partner/AddUser', data);
        }

        /**
         * @ngdoc method
         * @name stats
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function removeUser(data) {
            return shApi.request('/1/Partner/RemoveUser', data);
        }

        /**
         * @ngdoc method
         * @name stats
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateUser(data) {
            return shApi.request('/1/Partner/UpdateUser', data);
        }

         /**
         * @ngdoc method
         * @name stats
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function listUsers(data) {
            return shApi.request('/1/Partner/ListUsers', data);
        }

         /**
         * @ngdoc method
         * @name stats
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateBranding(data) {
            return shApi.request('/1/Partner/UpdateBranding', data);
        }

         /**
         * @ngdoc method
         * @name stats
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function getBranding(data) {
            return shApi.request('/1/Partner/GetBranding', data);
        }

         /**
         * @ngdoc method
         * @name stats
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function getReports(data) {
            return shApi.request('/1/Partner/GetReports', data);
        }

         /**
         * @ngdoc method
         * @name stats
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function getReportInfo(data) {
            return shApi.request('/1/Partner/GetReportInfo', data);
        }

        /**
         * @ngdoc method
         * @name stats
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function changeReportStatus(data) {
            return shApi.request('/1/Partner/ChangeReportStatus', data);
        }

        /**
         * @ngdoc method
         * @name GetSourceStats
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function getSourceStats(data) {
            return shApi.request('/1/Partner/GetSourceStats', data);
        }

        /**
         * @ngdoc method
         * @name CreateOrder
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function createOrder(data) {
            return shApi.request('/1/Partner/CreateOrder', data);
        }

        /**
         * @ngdoc method
         * @name ChangeOrderState
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function changeOrderState(data) {
            return shApi.request('/1/Partner/ChangeOrderState', data);
        }

        /**
         * @ngdoc method
         * @name ListOrders
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function listOrders(data) {
            return shApi.request('/1/Partner/ListOrders', data);
        }

        /**
         * @ngdoc method
         * @name GetAppspacePlans
         * @methodOf sh.api.shPartner
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function getAppspacePlans(data) {
            return shApi.request('/1/Partner/GetAppspacePlans', data);
        }
    }
})();
