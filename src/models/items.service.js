(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shItems', ItemsFactory);

    /**
     * @ngdoc service
     * @name sh.api.shItems
     * @description
     *
     * The `shItems` service provides a convenient wrapper for subusers related requests.
     *
     */

    /* @ngInject */
    function ItemsFactory(shApi) {

        var service = {
            list: list,
            rejected:rejected,
            restore:restore,
            approve:approve,
            setFeaturedStatus:setFeaturedStatus,
            ugcPost:ugcPost,
            ugcCancelPost:ugcCancelPost,
            ugcList:ugcList

        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name list
         * @methodOf sh.api.shItems
         *
         * @description
         * This function will list all sub users.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Items/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return shApi.request('/0/Items/List', data);
        }

        /**
         * @ngdoc method
         * @name rejected
         * @methodOf sh.api.shItems
         *
         * @description
         * This function will list all sub users.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Items/Rejected
         * ```
         *
         * @param {Object} data data options object.
         */
        function rejected(data) {
            return shApi.request('/0/Items/Rejected', data);
        }

        /**
         * @ngdoc method
         * @name restore
         * @methodOf sh.api.shItems
         *
         * @description
         * This function will list all sub users.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Items/Restore
         * ```
         *
         * @param {Object} data data options object.
         */
        function restore(data) {
            return shApi.request('/0/Items/Restore', data);
        }

        /**
         * @ngdoc method
         * @name approve
         * @methodOf sh.api.shItems
         *
         * @description
         * This function will list all sub users.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Items/Approve
         * ```
         *
         * @param {Object} data data options object.
         */
        function approve(data) {
            return shApi.request('/0/Items/Approve', data);
        }

        /**
         * @ngdoc method
         * @name setFeaturedStatus
         * @methodOf sh.api.shItems
         *
         * @description
         * This function will list all sub users.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Items/setFeaturedStatus
         * ```
         *
         * @param {Object} data data options object.
         */
        function setFeaturedStatus(data) {
            return shApi.request('/0/Items/setFeaturedStatus', data);
        }

        /**
         * @ngdoc method
         * @name ugcList
         * @methodOf sh.api.shItems
         *
         * @description
         * this function lists ugc entries for the user
         * Needs HubID
         *
         * ```js
         * https://api.open-display.io/webapi/core/Items/ugcList
         * ```
         *
         * @param {Object} data data options object.
         */
        function ugcList(data) {
            return shApi.request('/0/Items/UGCList', data);
        }

        /**
         * @ngdoc method
         * @name ugcPost
         * @methodOf sh.api.shItems
         *
         * @description
         * This function creates a UGC request in the database for List functions
         * needs NetworkID
         * ItemID
         * ConnectionID
         * HubID
         * PostURL
         * returns:
         * UGCID - item entry ID (attach this to item for future requests to cancel etc)
         * ```js
         * https://api.open-display.io/webapi/core/Items/ugcPost
         * ```
         *
         * @param {Object} data data options object.
         */
        function ugcPost(data) {
            return shApi.request('/0/Items/UGCPost', data);
        }

        /**
         * @ngdoc method
         * @name ugcCancelPost
         * @methodOf sh.api.shItems
         *
         * @description
         * This cancels the UGC Request made.
         *
         * UGCID - id of item
         *
         * ```js
         * https://api.open-display.io/webapi/core/Items/ugcCancelPost
         * ```
         *
         * @param {Object} data data options object.
         */
        function ugcCancelPost(data) {
            return shApi.request('/0/Items/UGCCancelPost', data);
        }
    }
})();
