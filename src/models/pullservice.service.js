(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shPullservice', PartnerFactory);

    /**
     * @ngdoc service 
     * @name sh.api.shPullservice
     * @description
     *
     * The `shPullservice` service provides a convenient wrapper for subusers related requests.
     *
     */

    /* @ngInject */
    function PartnerFactory(shApi) {

        var service = {
            skipItem: skipItem,
            startSync: startSync,
            getProcesses: getProcesses,
            getClientStatus: getClientStatus,
            getErrors: getErrors
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name skipItem
         * @methodOf sh.api.shPullservice
         *
         * @description
         * This function will allow you skipItem a sub user, you do need to skipItem a domain and group beforehand.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function skipItem(data) {
            return shApi.request('/1/Pullservice/SkipItem', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf sh.api.shPullservice
         *
         * @description
         * This function will list all sub users.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function startSync(data) {
            return shApi.request('/1/Pullservice/StartSync', data);
        }


        /**
         * @ngdoc method
         * @name remove
         * @methodOf sh.api.shPullservice
         *
         * @description
         * This function will allow you to remove the sub user.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function getProcesses(data) {
            return shApi.request('/1/Pullservice/GetProcesses', data);
        }

        /**
         * @ngdoc method
         * @name update
         * @methodOf sh.api.shPullservice
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function getClientStatus(data) {
            return shApi.request('/1/Pullservice/GetClientStatus', data);
        }

        /**
         * @ngdoc method
         * @name stats
         * @methodOf sh.api.shPullservice
         *
         * @description
         * This function will update the user info.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Partner/Update
         * ```
         *
         * @param {Object} data data options object.
         */
        function getErrors(data) {
            return shApi.request('/1/Pullservice/GetErrors', data);
        }
    }
})();
