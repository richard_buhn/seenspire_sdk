(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('shGroups', groupsFactory);

    /**
     * @ngdoc service 
     * @name sh.api.shGroups
     * @description
     *
     * The `shGroups` service provides a convenient wrapper for group related requests.
     *
     */

    /* @ngInject */
    function groupsFactory(shApi) {

        var service = {
            addUserToGroup: addUserToGroup,
            create: create,
            list: list,
            remove: remove,
            removeUserFromGroup: removeUserFromGroup,
            updateItem: updateItem
        };

        return service;

        ////////////////

        /**
         * @ngdoc method
         * @name addUserToGroup
         * @methodOf sh.api.shGroups
         *
         * @description
         * This function allows you to add a user to a group.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Groups/AddUserToGroup
         * ```
         *
         * @param {Object} data data options object.
         */
        function addUserToGroup(data) {
            return shApi.request('/0/Groups/AddUserToGroup', data);
        }

        /**
         * @ngdoc method
         * @name create
         * @methodOf sh.api.shGroups
         *
         * @description
         * This function will allow you to create a group. Groups are used for accessing data and functions. 
         * You can restrict access to part of the system and allow read and write privileges to files,folders,slides,devices.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Groups/New
         * ```
         *
         * @param {Object} data data options object.
         */
        function create(data) {
            return shApi.request('/0/Groups/New', data);
        }

        /**
         * @ngdoc method
         * @name list
         * @methodOf sh.api.shGroups
         *
         * @description
         * This function will list all of the groups.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Groups/List
         * ```
         *
         * @param {Object} data data options object.
         */
        function list(data) {
            return shApi.request('/0/Groups/List', data);
        }

        /**
         * @ngdoc method
         * @name remove
         * @methodOf sh.api.shGroups
         *
         * @description
         * This function will remove the group.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Groups/Remove
         * ```
         *
         * @param {Object} data data options object.
         */
        function remove(data) {
            return shApi.request('/0/Groups/Remove', data);
        }

        /**
         * @ngdoc method
         * @name removeUserFromGroup
         * @methodOf sh.api.shGroups
         *
         * @description
         * This function will remove the user from the group.
         *
         * ```js
         * https://api.open-display.io/webapi/core/Groups/RemoveUserFromGroup
         * ```
         *
         * @param {Object} data data options object.
         */
        function removeUserFromGroup(data) {
            return shApi.request('/0/Groups/RemoveUserFromGroup', data);
        }

        /**
         * @ngdoc method
         * @name updateItem
         * @methodOf sh.api.shGroups
         *
         * @description
         * This function will allow you to set a group or groups to a file, folder, device, playlist or slide.
         *
         * The following types are available: 
         * File, Slide, Device, FileFolder, SlideFolder, DeviceFolder, Playlists
         *
         * Items are the ids of the type you want to set the groups to.
         * Groups are a list of group ids
         *
         * ```js
         * https://api.open-display.io/webapi/core/Groups/UpdateItem
         * ```
         *
         * @param {Object} data data options object.
         */
        function updateItem(data) {
            return shApi.request('/0/Groups/UpdateItem', data);
        }
    }
})();
