(function() {
    'use strict';

    angular
        .module('sh.api')
        .factory('logger', logger);

    /* @ngInject */
    function logger() {

        var service = {
            apiError: apiError,
            error: error,
            info: info,
            success: success,
            warning: warning
        };

        return service;

        ////////////////

        function apiError(errorCode, message, data) {
            /*
            $mdToast.show($mdToast.simple()
                .content(message)
                .position('top right')
            );
            */
        }

        function error(message, data) {

        }

        function info(message, data) {

        }

        function success(message, data) {

        }

        function warning(message, data) {

        }
    }
})();
