(function() {
    'use strict';

    angular
        .module('sh.api')
        .constant('shConfig', {
            APP_ID: '',
            AUTH_URL: 'https://api.seenspire.social/login',
            API_URL: 'https://api.seenspire.social/webapi',
            AUTO_REDIRECT: true
        })
        .service('shApi', ApiService);

    /* @ngInject */
    function ApiService($http, shConfig, $cookies, logger, $rootScope, $location, $q) {
        // init api service vars
        var socialHub = this;

        socialHub.auth = false;
        socialHub.token = $cookies.get('shToken');

        //local development server
        socialHub.devs = ["35.189.68.200", "35.187.80.56"];
        socialHub.devIPSet = false;

        // link functions
        socialHub.setToken = setToken;
        socialHub.login = login;
        socialHub.request = request;
        socialHub.logout = logout;
        socialHub.getReferrer = getReferrer;
        socialHub.AppOauth = AppOauth;

        activate();

        function $_GET(parameterName) {
            var result = null,
                tmp = [];
            location.search
            .substr(1)
                .split("&")
                .forEach(function (item) {
                tmp = item.split("=");
                if (tmp[0] === parameterName) {
                    result = decodeURIComponent(tmp[1]);
                }
            });
            return result;
         }

        function devCheck(){
            if($_GET('devip') != null){
                return true;
            }

            return false;
        }

        function devIP(){
            if($_GET('devip') != null){
                return $_GET('devip');
            }

            return false;
        }

        function activate() {
            if(devCheck()){
                setDevIP(devIP());
            }
            
            // lets validate that the request is from the valide domains and set the token
            var TempToken = location.href.substr(location.href.length - 128);
            if (TempToken.length === 128) {
                socialHub.setToken(TempToken);
            
                var oldState = getState();

                if(oldState === ''){
                    var stringLocation = location.href;
                    stringLocation = stringLocation.replace('#' + TempToken, '');
                    stringLocation = stringLocation.replace('#/' + TempToken, '');
                    stringLocation = stringLocation.replace(TempToken, '');
                    window.location.href = stringLocation;
                }else{
                   //set redirect state back to default
                   setState('');

                   //go back to the current state          
                   $location.url(oldState);
                }
            }
        }

        //////////////////////////////

        //set dev ip if needed
        function setupDevIP(){
            if(!socialHub.devIPSet){
                var IP = getDevIP();
                if(IP !== ""){
                    shConfig.AUTH_URL = 'http://'+IP+'/login';
                    shConfig.API_URL = 'http://'+IP+'/webapi';
                }

                socialHub.devIPSet = true;
            }
        }

        // set the session token and add a cookie
        function setToken(token) {
            $cookies.put('shToken', token);
            socialHub.token = token;
        }

        // set the dev var
        function setDevIP(IP) {
            if(IP != "false"){
                $cookies.put('shDevIP', IP);
            }else{
                $cookies.put('shDevIP', '');
            }
        }

        function getDevIP(){
           var IP = $cookies.get('shDevIP');
           if(IP !== undefined){
              if(IP !== ''){
                  return IP;
              }
           }

           return ''; 
        }

        function setState(location){
            $cookies.put('shState', location);

            if(location !== ''){
                //set a cookie before we go to the login page so we know who is accessing the app
                $cookies.put('shReferrer', document.referrer);       
            }      
        }

        function getReferrer(){
           var url = $cookies.get('shReferrer');
           if(url !== undefined){
              if(url !== ''){
                  return url;
              }
           }

           return ''; 
        }

        function getState(){
           var url = $cookies.get('shState');
           if(url !== undefined){
              if(url !== ''){
                  return url;
              }
           }

           return '';
        }

        // redirect to the login page
        function login() {
            var URL = '';
            
            if (shConfig.APP_ID !== '') {
                URL = shConfig.AUTH_URL + '?AppID=' + shConfig.APP_ID;
            } else {
                URL = shConfig.AUTH_URL + '?RedirectURL=' + encodeURIComponent($location.absUrl() + '#');
            }
            if (shConfig.AUTO_REDIRECT) {
                window.location.href = URL;
            }
        }

        function AppOauth(AppID, Key){
            var deferred = $q.defer();

            //setup the dev ip for making request to the dev API 
            setupDevIP();

            $rootScope.$broadcast('event:socialHubRequest', 'start');

            $http({
                method: 'POST',
                url: shConfig.API_URL + "/0/Authenticate/AuthAppUser",
                transformRequest: formatData,
                data: {
                    RequestData: angular.toJson({
                        AppID: AppID,
                        Key: Key
                    }) 
                },
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).then(done).catch(error);

            // on request done we handel the request
            function done(response) {
                $rootScope.$broadcast('event:socialHubRequest', 'end');
                if (response.data.ResponseHead.Code === 200) {
                    // lets send the event that we are logged in
                    if (!socialHub.auth) {
                        $rootScope.$broadcast('event:socialHubAuth', true);
                    }
                    socialHub.setToken(response.data.Session.Token);
                    socialHub.auth = true;
                    deferred.resolve(response.data);
                } else {
                    deferred.reject(response.data);
                }
            }

            // if there is a connectin error we handel the message
            function error(response) {
                $rootScope.$broadcast('event:socialHubRequest', 'end');
                logger.error(response);
            }

            // format the request to the server
            function formatData(obj) {
                var str = [];
                for (var p in obj) {
                    if (obj.hasOwnProperty(p)) {
                        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                    }
                }
                return str.join('&');
            }

            return deferred.promise;
        }

        // lets make a api request
        function request(url, data) {
            var deferred = $q.defer();

            //setup the dev ip for making request to the dev API 
            setupDevIP();
            
            $rootScope.$broadcast('event:socialHubRequest', 'start');

            $http({
                method: 'POST',
                url: shConfig.API_URL + url,
                transformRequest: formatData,
                data: {
                    Token: socialHub.token,
                    RequestData: data ? angular.toJson(data) : angular.toJson({})
                },
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).then(done).catch(error);

            // on request done we handel the request
            function done(response) {
                $rootScope.$broadcast('event:socialHubRequest', 'end');
                if (response.data.ResponseHead.Code !== 401) {
                    // lets send the event that we are logged in
                    if (!socialHub.auth) {
                        $rootScope.$broadcast('event:socialHubAuth', true);
                    }

                    socialHub.auth = true;

                    if (response.data.ResponseHead.Code !== 200) {
                        deferred.reject(response.data);
                        logger.apiError(response.data.ResponseHead.Code, response.data.ResponseHead.Message, response.data);
                    } else {
                        deferred.resolve(response.data);
                    }
                } else {
                    var token = $cookies.get('shToken');
                    if(!token) {
                        setState($location.url());
                    } else {
                        setState('');
                    }

                    socialHub.auth = false;
                    socialHub.setToken('');

                    //if the domain is senspire.social we redirect to the subdomain login page
                    if(window.location.hostname.match(/\.seenspire\.social/g) &&
                        !window.location.hostname.match(/beta\.seenspire\.social/g) &&
                        !window.location.hostname.match(/partner\.seenspire\.social/g) &&
                        !window.location.hostname.match(/betapartner\.seenspire\.social/g)){
                        shConfig.AUTH_URL = "https://"+window.location.hostname+"/login";
                    }

                    socialHub.login();
                }
            }

            // if there is a connectin error we handel the message
            function error(response) {
                $rootScope.$broadcast('event:socialHubRequest', 'end');
                logger.error(response);
            }

            // format the request to the server
            function formatData(obj) {
                var str = [];
                for (var p in obj) {
                    if (obj.hasOwnProperty(p)) {
                        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                    }
                }
                return str.join('&');
            }

            return deferred.promise;
        }

        // clear cookie and redirect tot the login page
        function logout() {
            if(!$location.url().match(/\/create\//g)){
                setState('');
            } else {
                setState($location.url());
            }
            socialHub.setToken('');
            socialHub.login();
        }
    }
})();
