NG_DOCS={
  "sections": {
    "api": "API Documentation"
  },
  "pages": [
    {
      "section": "api",
      "id": "sh.api.shAccount",
      "shortName": "sh.api.shAccount",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shAccount service provides a convenient wrapper for account related requests.",
      "keywords": "account accountid additional allow allows angular api apiinfo apiversion branding cancelaccount class code console controller convenient current custom data decide developer displays examplecontroller flexible function globalsettings hours https info inside io js list live log main match message method module object oldpassowrd open-display options password passwordcheck profile request requestdata requestdisplays requestlivehours requests response responsebody responsehead retrieve return send servertime service session settings sh shaccount stats store stored storing structure subaccountid succesfully token update updateaccountexample updated updateglobalsettings updatepassword updatesettings user useremail username usertype validation var wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shApplication",
      "shortName": "sh.api.shApplication",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shApplication service provides a convenient wrapper for app related requests.",
      "keywords": "access accesslevels account allow allowed anytime api app application applications categories control convenient create custom data default devlist function generatelonglivedtoken grant group groups https io js list located method object open-display options remove requests revokeaccess revoked rules service setgroup settings sh shapplication subuser subusers system update user users wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shAuthenticate",
      "shortName": "sh.api.shAuthenticate",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shAuthenticate service provides a convenient wrapper for authentication.",
      "keywords": "allows api appid authentication behalf called check client convenient data destroyed function https io js key login logout method object open-display options private receive request response send server service sh shauthenticate side token valid wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shBilling",
      "shortName": "sh.api.shBilling",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shBilling service provides a convenient wrapper for account related requests.",
      "keywords": "account api branding calculatepricing convenient data function info method object options requests return service sh shbilling stats wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shClients",
      "shortName": "sh.api.shClients",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shClients service provides a convenient wrapper for subusers related requests.",
      "keywords": "allow api changelicensesamount convenient create data domain function getaccountinfo getaccountsettings getclientsettings getinfotainmentstats getinvoiceurl group https info io js list listdisplays listsubusers method move object open-display options remove removedisplay renamedisplay requests restore saveclientsettings service setdisplaystate sh shclients subusers update updateappspaceplan updateextrafeeds updatelivehours updatetrial user users wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shDisplay",
      "shortName": "sh.api.shDisplay",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shDisplay service provides a convenient wrapper for subusers related requests.",
      "keywords": "allow api convenient create data domain function group https io js list method object open-display options remove rename requests service setstate sh shdisplay subusers user wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shDomains",
      "shortName": "sh.api.shDomains",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shDomains service provides a convenient wrapper for domain related requests.",
      "keywords": "add addusertodomain allow api authenticating convenient create data domain domains exist function global grouping https io js list meaning method object open-display options remove removeuserfromdomain requests service sh shdomains system user wide wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shGroups",
      "shortName": "sh.api.shGroups",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shGroups service provides a convenient wrapper for group related requests.",
      "keywords": "access accessing add addusertogroup allow allows api convenient create data device devicefolder devices file filefolder files folder folders function functions group groups https ids io items js list method object open-display options playlist playlists privileges read remove removeuserfromgroup requests restrict service set sh shgroups slide slidefolder slides system type types updateitem user wrapper write"
    },
    {
      "section": "api",
      "id": "sh.api.shHub",
      "shortName": "sh.api.shHub",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shHub service provides a convenient wrapper for subusers related requests.",
      "keywords": "allow api based channels convenient create data domain function group https ids info io js list method names object open-display options query remove requests return service sh shhub statistics subusers twitter twitterautocomplete update user users wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shInfotainment",
      "shortName": "sh.api.shInfotainment",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shInfotainment service provides a convenient wrapper for infotainment related requests.",
      "keywords": "aircheckrcountries aircheckrlocations allow api approve convenient create currencies currenciescombinationactive data datacallrequest domain feature function getsources getstocks group https infotainment io js list listitems method object open-display options reject remove requests restore service sh shinfotainment user wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shItems",
      "shortName": "sh.api.shItems",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shItems service provides a convenient wrapper for subusers related requests.",
      "keywords": "api approve cancel cancels connectionid convenient creates data database entries entry function functions future https hubid io item itemid js list lists method networkid object open-display options posturl rejected request requests restore returns service setfeaturedstatus sh shitems subusers ugc ugccancelpost ugcid ugclist ugcpost user users wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shLog",
      "shortName": "sh.api.shLog",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shLog service provides a convenient wrapper for log related requests.",
      "keywords": "actions allow api convenient data function https io js list log login logins method object open-display options requests service sh shlog wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shNetworks",
      "shortName": "sh.api.shNetworks",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shNetworks service provides a convenient wrapper for subusers related requests.",
      "keywords": "allow api convenient create data domain function group https info io js list method object open-display options remove requests service sh shnetworks subusers update user users wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shPartner",
      "shortName": "sh.api.shPartner",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shPartner service provides a convenient wrapper for subusers related requests.",
      "keywords": "allow api changeorderstate convenient create createorder data domain function getappspaceplans getsourcestats group https info io js list listorders method object open-display options remove requests service sh shpartner stats subusers update user users wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shPlatformStats",
      "shortName": "sh.api.shPlatformStats",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shPlatformStats service provides a convenient wrapper for subusers related requests.",
      "keywords": "allow api convenient data domain function global group https io js method object open-display options requests service sh shplatformstats skipitem subusers user wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shPullservice",
      "shortName": "sh.api.shPullservice",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shPullservice service provides a convenient wrapper for subusers related requests.",
      "keywords": "allow api convenient data domain function group https info io js list method object open-display options remove requests service sh shpullservice skipitem stats subusers update user users wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shSubUsers",
      "shortName": "sh.api.shSubUsers",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shSubUsers service provides a convenient wrapper for subusers related requests.",
      "keywords": "allow api convenient create data domain function group https info io js list method object open-display options password remove requests retrieve service sh shsubusers subusers update updatepassword user users wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shSystem",
      "shortName": "sh.api.shSystem",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shSystem service provides a convenient wrapper for subusers related requests.",
      "keywords": "allow api convenient create data domain function group https io js list method object open-display options requests service sh shsystem subusers update user users wrapper"
    },
    {
      "section": "api",
      "id": "sh.api.shUGC",
      "shortName": "sh.api.shUGC",
      "type": "service",
      "moduleName": "sh.api",
      "shortDescription": "The shUGC service provides a convenient wrapper for subusers related requests.",
      "keywords": "api check checktimestamp convenient current data decline entries filters function https insert io js list method object open-display options request requests service sh shugc subusers time ugc wrapper"
    }
  ],
  "apis": {
    "api": true
  },
  "html5Mode": false,
  "editExample": true,
  "startPage": "/api",
  "scripts": [
    "angular.min.js"
  ]
};